window.requestAnimFrame = (function(){
      return  window.requestAnimationFrame       || 
              window.webkitRequestAnimationFrame || 
              window.mozRequestAnimationFrame    || 
              window.oRequestAnimationFrame      || 
              window.msRequestAnimationFrame     || 
              function( callback, element){
                window.setTimeout(callback, 1000 / 60);
              };
    })();

    var connection = null;
    //Récupération du canvas de jeu dans le HTML 
    gameCanvas = document.getElementById('gameCanvas');
    gameContext = gameCanvas.getContext('2d');
    gameCanvasWidth = 800;
    gameCanvasHeight = 500;

    //Déclaration des dimensions d'affichage d'une voiture
    width = 60;
    height = 38;
    
    //Gestion des voitures et de leurs états de déplacement
    cars = new Array();
    myId = null;
    acceleration = 0;
    rotation = 0;
    oldAcceleration = 0;
    oldRotation = 0;
    
    //Gestion des booléens représentatifs des touches du clavier/gamepad
    keys = {
        up : false,
        down : false,
        left : false,
        right : false
    }

    //Gestion des photos du dernier perdant
    photoCanvas = document.getElementById("photoCanvas");
    photoCanvasWidth = 130;
    photoCanvasHeight = 103;
    photoCanvasContext = photoCanvas.getContext("2d");
    //Variable contenant l'entête a retirer et ajouter des images reçues pour les afficher.
    image_header = "data:image/png;base64,";
    video = document.querySelector("#videoElement");
    
    //Fonction d'initialisation du jeu
    function init(){
        gameContext.clearRect(0, 0, gameCanvasWidth, gameCanvasHeight);
        keys["up"] = false;
        keys["down"] = false;
        keys["left"] = false;
        keys["right"] = false;
        sendActions();
    }

    //Fonction d'envoi des données appelée continuellement
    function sendActions() {
        if(keys["up"]){
            acceleration = 1;
        }
        if(keys["down"]){
            acceleration = -1;
        }
        if((keys["up"] && keys["down"]) 
        || (!keys["up"] && !keys["down"])) {
            acceleration = 0;
        }
        if(keys["left"]){
            rotation = -1;
        }
        if(keys["right"]){
            rotation = 1;
        }
        if((keys["right"] && keys["left"]) 
        || (!keys["right"] && !keys["left"])) {
            rotation = 0;
        }

        //On envoie le changement de direction/vitesse au serveur uniquement s'ils ont changé par 
        //rapport à la valeur envoyée précédemment
        if(acceleration != oldAcceleration || rotation != oldRotation){
            oldAcceleration = acceleration;
            oldRotation = rotation;
            var movement = {id : myId, acceleration : acceleration, rotation : rotation};
            var message = JSON.stringify({'type': 'movement', 'data': movement});
            connection.send(message);
        }
        requestAnimFrame(sendActions);
    }

   /*Création d'une voiture           *
    * Paramètres :                    *
    * identifiant,                    *
    * position en x,                  *
    * position en y,                  *
    * rotation actuelle de la voiture */
    function Car (id, x, y, carRotation) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.rotation = carRotation;
        //On récupère l'image en fonction de l'identifiant
        this.imageNumber = id%5;
        this.image = new Image();
        this.image.src = "car"+this.imageNumber+".gif";
    }

    //Dessin de la voiture selon ses coordonnées
    var drawCar = function(car)
    {
        gameContext.save();
        gameContext.translate(car.x, car.y);
        gameContext.rotate(Math.PI/180 * car.rotation);
        gameContext.drawImage(car.image, -(width/2), -(height/2), width, height);   
        gameContext.restore();
    }

    //Gestion du contrôle de la voiture via les touches du clavier appuyées
    function keyPressed(e){
        //Si Z
        if(e.keyCode == 90){
            keys["up"] = true;
        }
        //Si Q
        if(e.keyCode == 81){
            keys["left"] = true;
        }
        //Si S
        if(e.keyCode == 83){
            keys["down"] = true;
        }
        //Si D
        if(e.keyCode == 68){
            keys["right"] = true;
        }
    }

    //Gestion du contrôle de la voiture via les touches du clavier relachées
    function keyReleased(e){
        //Si Z
        if(e.keyCode == 90){
            keys["up"] = false;
        }
        //Si Q
        if(e.keyCode == 81){
            keys["left"] = false;
        }
        //Si S
        if(e.keyCode == 83){
            keys["down"] = false;
        }
        //Si D
        if(e.keyCode == 68){
            keys["right"] = false;
        }
    }

    // Gestion du gamePad
    var hasGP = false; //est ce que le gamePad est connecté?
    var repGP;
 
    //Le navigateur reconnait il cette propriété?
    function canGame() {
        return "getGamepads" in navigator;
    }
 
    function reportOnGamepad() {
        var gp = navigator.getGamepads()[0];//on peut connecter plus d'un gamepad mais on ne teste que le premier ici.
        var html = "";
        //Si on appuie sur gauche avec la directionnelle
        if(gp.axes[4] == -1){
            keys["left"] = true;
        }
        //Si on appuie sur droite
        else if(gp.axes[4] == 1){
            keys["right"] = true;
        }
        //Si on appuie ni sur gauche, ni sur droite
        else{
            keys["left"] = false;
            keys["right"] = false;
        }

        //Si on appuie sur haut avec la directionnelle
        if(gp.axes[5] == -1){
            keys["up"] = true;
        }
        //Si on appuie sur bas
        else if(gp.axes[5] == 1){
            keys["down"] = true;
        }
        //Si on appuie ni sur haut, ni sur bas
        else{
            keys["up"] = false;
            keys["down"] = false;
        }        
 
        $("#gamepadDisplay").html(html); //Affichage
    }// voyez que cette fonction n'est pas rattachée à un événement: elle sera appelée régulièrement par un setInterval (cf plus bas).
 
 
    //fonction principale lancée au chargement complet de la page 
    $(document).ready(function() {
 
        if(canGame()) { //on vérifie si le navigateur est suffisament récent.
            var prompt = "Pour jouer, utilisez les touches Z, Q, S et D pour déplacer la voiture, ou branchez votre gamePad et déplacez la avec les flèches directionnelles.";
            $("#gamepadPrompt").text(prompt);

            $(window).on("gamepadconnected", function() { //rattachement  de l'événement (cf plus bas) à une callback
                hasGP = true; //on garde en mémoire qu'il existe un gamePad
                $("#gamepadPrompt").html("Votre gamepad est connecté ! Déplacez la voiture avec les directionnelles.");
                console.log("connection event");
                
                //Ceci est une boucle de jeu
                repGP = window.setInterval(reportOnGamepad,100); //On interroge régulièrement le gamePad pour connaître ses variations d'état
                //Dans l'idéal, on utilise pas setInterval mais requestAnimationFrame (cf http://creativejs.com/resources/requestanimationframe/)
            });

            $(window).on("gamepaddisconnected", function() {//récupération de l'événement (cf plus bas)
                console.log("disconnection event");
                $("#gamepadPrompt").text(prompt);
                window.clearInterval(repGP);
            });
 
            //Chrome nécessite un petit interval, Firefox, non, mais cela ne dérange pas.
            var checkGP = window.setInterval(function() {
                if(navigator.getGamepads()[0]) {
                    if(!hasGP) $(window).trigger("gamepadconnected"); //déclenchement de l'événement
                    window.clearInterval(checkGP);
                }
            }, 500);

            // Gestion de la connexion au serveur et de l'envoi de messages
            //Connexion au serveur
            var serverPort = 80;
            window.WebSocket = window.WebSocket || window.MozWebSocket;

            // Si le navigateur ne supporte pas les WebSocket, on affiche un message avant de quitter
            if (!window.WebSocket) {
                alert("Sorry, but your browser doesn\'t support WebSockets");
                return;
            }

            // Connexion
            connection = new WebSocket('ws:'+askUserRemoteServerAddress());

            connection.onopen = function () {
            };

            connection.onerror = function (error) {
            };

            // Réception de message
            connection.onmessage = function (message) {
                try {
                    var json = JSON.parse(message.data);
                } catch (e) {
                    console.log('This doesn\'t look like a valid JSON: ', message.data);
                    return;
                }
                //Si on reçoit un message d'affichage des voitures, on les redessine une par une
                if (json.type === 'DessinerVoitures') {
                    gameContext.clearRect(0, 0, gameCanvasWidth, gameCanvasHeight);
                    cars = new Array();
                    //On récupère la liste des voitures à dessiner (on ne récupère pas les voitures n'étant plus en course)
                    for (var carId in carsList = json.data){
                        if(carsList[carId] != undefined && carsList[carId].isAlive == true){
                            cars.push(new Car(carsList[carId].id, carsList[carId].x, carsList[carId].y, carsList[carId].rotation));
                        }
                    }
                    //On dessine chacune des voitures précédemment récupérées
                    for (var carNumber in cars){
                        drawCar(cars[carNumber]);   
                    } 
                }

                //Si on reçoit un message d'identification, on récupère notre identifiant pour savoir quelle voiture nous sommes
                if (json.type === 'Identification') {
                    myId = json.data;
                    init();
                }

                //Si on reçoit un message qui dit qu'on a perdu, on envoie notre photo à l'instant T au serveur
                if (json.type === 'Perdu') {
                        //On la dessine dans notre canvas en local avant d'envoyer cette photo au serveur
                        photoCanvasContext.drawImage(video, 0, 0, photoCanvasWidth, photoCanvasHeight);
                        var img = photoCanvas.toDataURL("image/png");
                        //Il faut supprimer l'entete afin d'envoyer uniquement le contenu du canvas a l'instant T
                        img = img.replace(/^data:image\/(png|jpg);base64,/, "");
                        var data = {"id" : myId, "image" : img};
                        var message = JSON.stringify({'type': 'imagePerdant', 'data': data});
                        //On envoie notre identifiant et le contenu du canvas
                        connection.send(message);
                }

                //Si l'on reçoit un message contenant la photo du dernier perdant, on l'affiche dans notre canvas
                if (json.type === 'imagePerdant') {
                    //On rajoute l'entête retiré précedemment
                    var imgDataUrl = image_header+json.data;
                    var img = new Image();
                    img.src = imgDataUrl;
                    //On dessine la photo dans le canvas
                    img.onload = function() {
                        photoCanvasContext.drawImage(img, 0, 0, photoCanvasWidth, photoCanvasHeight);
                    };
                }
            };

            //Gestion du flux vidéo pour l'envoi de la dernière photo du perdant
            function handleVideo(stream) {
                video.src = window.URL.createObjectURL(stream);
            }
             
            function videoError(e) {
                // do something
            }
 
            navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;

            if (navigator.getUserMedia) {       
                navigator.getUserMedia({video: true}, handleVideo, videoError);
            }
        }
    });

    //Ajout des evènements claviers au jeu
    window.addEventListener("keydown", keyPressed, true);
    window.addEventListener("keyup", keyReleased, false);

    //Retourne l'adresse du serveur webSocket
    var askUserRemoteServerAddress = function() {
        var address = window.prompt("Please enter remote server address", "localhost:80");
        return address;
    };