//========================
// Fonctions utilitaires
//========================

//Permet de logger un message dans la console
var logConsole = function(message) {
	var date = new Date();
	var formatedMessage = '['+date.getHours()+':'+date.getMinutes()+':'+date.getSeconds()+'] '+message;
	console.log(formatedMessage);
};

//===============================
// Configuration du serveur web
//===============================

// constantes & dépendances
var WebSocketServer = require('websocket').server;
var express         = require('express');
var app             = express();
var port 			= 80; // port d'écoute du serveur
var canvasHeight = 500;
var canvasWidth = 800;
var carSpeed = 2;
cars = new Array();

// création du serveur web
var webServer = app.listen(port);

// Rendre accessible aux clients le répertoire public (scripts, images, ... pour le client)
app.use(express.static(__dirname + '/public'));

// configuration des routes
app.get('/', function(req,res) {
	res.sendFile('client.html', { root: __dirname+"/public" });
});

//==================================
// Configuration websocketServer
//==================================

// création du webSocketServer (qui utilise le même port que le webServer, d'où le webServer en paramètre)
var webSocketServer = new WebSocketServer({ httpServer : webServer });
logConsole('websocket Server successfuly started, listening on port : '+port);

// liste des clients connectés
var clients = [];

// Appelé à chaque connexion d'un client
webSocketServer.on('request', function(request) {

	// accepte la connexion
    var connection = request.accept(null, request.origin);

    // pour y accéder plus tard (dans la méthode connection)
    var savedConnection = connection;

    // Création du client
    var newClient = new Client(guid(), connection);

    // associe le client à sa connexion (socket)
    associateConnectionToClient(newClient);
    logConsole('client (ID : '+newClient.guid+') connected from : '+newClient.connection.remoteAddress);

    // ajout du client à la liste
    clients[newClient.guid] = newClient;
    
    // Envoi du message d'identification au nouveau client
    new Message('Identification', newClient.guid).to(newClient).send();
    
    // Envoi du message d'initialisation à tous les clients
    var newCar = new Car (newClient.guid, 0, 0, 0, 0);
    cars[newClient.guid] = newCar;
    reinitialisation();

    // Lorsque le serveur reçoit un message
    connection.on('message', function(inputMessage) {
    	var client = getClientFromConnection(connection);
    	var message = parseIncomingMessage(inputMessage);

    	//Quand on reçoit un message qui dit que la vitesse/direction a changé
    	if(message.type === "movement"){
    		//On met à jour les caractéristiques selon les mouvements du véhicule
    		switch (message.data.acceleration){
    			case 1:
    				cars[message.data.id].speedModification = 1.5;
    				break;
    			case 0:
    				cars[message.data.id].speedModification = 1;
    				break;
    			case -1:
    				cars[message.data.id].speedModification = 0.7
    				break;
    			default:
    				break;
    		}

    		switch (message.data.rotation){
    			case 1:
    				cars[message.data.id].rotationModification = 1;
    				break;
    			case -1:
    				cars[message.data.id].rotationModification = -1;
    				break;
    			case 0:
    				cars[message.data.id].rotationModification = 0;
    			default:
    				break;
    		}
    	}
    	
    	// Si on reçoit le message qui contient la photo du perdant
    	if(message.type === "imagePerdant"){
    		// On envoie le message aux autres joueurs que celui qui a envoyé la photo (il l'a déjà stockée)
    		new Message('imagePerdant', message.data.image).toOthers(client.guid).send();
    	}
    });

    // Si le client ferme la connexion, on le déconnecte
    connection.on('close', function(connection) {
    	var disconnectingClient = getClientFromConnection(savedConnection);
    	deleteDisconnectedClients(disconnectingClient);
    });
});

//---------------------------
// Classes 
//---------------------------
    
// Classe Client
var Client = function(guid, connection) {
	this.guid = guid;
	this.connection = connection;
};

// Classe Message 
var Message = function(type, message) {
	this.type = type; // le type du message
	this.data = message; // le message
	this.receivers = []; // destinataires (liste d'objets Client)

	// Envoie le message vers les destinataires
	this.send = function() {
		for(var i = 0; i < this.receivers.length; i++) {
			if(this.receivers[i] != undefined){
				this.receivers[i].connection.sendUTF(this.prepareMessage());
			}
		}
	};

	//---------------------------------------------
    // Fonctions qui spécifient les destinataires
	// du message
    //---------------------------------------------

	// Tout le monde
	this.toAll = function() {
		for(var i = 0; i < clients.length; i++) {
			if(clients[i] != undefined){
				this.receivers.push(clients[i]);
			}
		}
	    return this;
	};

	// Tout le monde sauf l'emetteur
	this.toOthers = function(client) {
		for(var i = 0; i < clients.length; i++) {
			if(clients[i] != undefined && clients[i].guid != client.guid) {
				this.receivers.push(clients[i]);
			}
		}
	    return this;
	};

	// Un client spécifique
	this.to = function(client) {
		for(var i = 0; i < clients.length; i++) {
			if(clients[i] != undefined){
				if(client.guid === clients[i].guid) {
					this.receivers.push(clients[i]);
				}
			}
		}
		return this;
	};

	//-------------------
    // Fonctions privées
    //-------------------

	// met en forme le message pour l'envoi
	this.prepareMessage = function(){
		return JSON.stringify({type: this.type, data: this.data});
	};
};

// Parcourt un message reçu par le serveur et retourne un objet Message en résultat
var parseIncomingMessage = function(inputMessage) {
	if(inputMessage.type === 'utf8') {
		var message = null;
		try {
        	var data = JSON.parse(inputMessage.utf8Data);
        	message = new Message(data.type, data.data);
    	} catch(e){
        	logConsole(e);
    	}	
		return message;
	} else {
		logConsole('UTF8 messages authorized only');
	}	
};

//---------------------------
// Méthodes
//---------------------------

// Associe un GUID à une socket pour savoir à quel client correspond la socket lors d'events
var associateConnectionToClient = function(client) {
	var connection = client.connection;
	connection['guid'] = client.guid;
};

// Récupère le client (objet) associé à la connection (socket) fournie en paramètre
var getClientFromConnection = function(connection) {
	for(var i = 0; i < clients.length; i++) {
		if(clients[i] != undefined){
			if(clients[i].guid === connection.guid) {
				return clients[i];
			}
		}
	}
};

// Supprime le client qui vient de se déconnecter de la liste
var deleteDisconnectedClients = function(client) {
	for(var i = 0; i < clients.length; i++) {
		if(clients[i] != undefined && clients[i].guid === client.guid){
			//On met les cases à "undefined" pour qu'elles soient
			//occupées par le prochain client qui va se connecter
			//Suppression de la voiture du client
			delete cars[i];
			//Suppression du client
			logConsole("Déconnexion du client n°" + clients[i].guid);
			delete clients[i];
		}
	}
};

// Détermine un client id (guid) unique
var guid = function() {
	var id = -1;
	var keepSearching = true;
	while(keepSearching) {
		id ++;
		keepSearching = false;
		for(var client in clients) {
			if(clients[client].guid === id) {
				keepSearching = true;
			}
		}
	}
	return id;
};

//====================================================
// Variables, fonctions et logique du jeu (StockCar)
//====================================================

function gameLoop(){
	//On ne fait rien s'il n'y a aucun client
	if(cars.length != 0){
		// On déplace chaque voiture en couse
		moveCars();

		// On applique les forces des potentielles collisions entre les voitures
		detectPlayersCollision();

		// S'il y a eu collision avec le bord du canvas
		if(detectBordersCollision()){		
			//On récupère le nombre de voitures restantes
		    var nbVoituresRestantes = 0;
		    for (carId in cars){
		        if (true == cars[carId].isAlive){
		           	nbVoituresRestantes += 1;
		        }
		    }

		    //S'il n'en reste qu'une ou moins (si les deux dernières voitures perdent en même temps, on réinitialise le jeu
		    if(nbVoituresRestantes <= 1){
				reinitialisation();
			}
		}
	}
	new Message('DessinerVoitures', cars).toAll().send();
}

/*Création d'une voiture           *
 * Paramètres :                    *
 * identifiant,                    *
 * position en x,                  *
 * position en y,                  *
 * rotation actuelle de la voiture *
 * vitesse de base de la voiture   */
 function Car (id, x, y, carRotation, carSpeed) {
     this.id = id;
     this.x = x;
     this.y = y;
     this.rotation = carRotation;
     this.speed = carSpeed;
     this.rotationModification = 0;
     //facteur d'acceleration de la voiture
     this.speedModification = 1;
     //la voiture est-elle en course ou a déjà perdu ?
     this.isAlive = false;
     //Force additionnelle de collision sur l'axe x
     this.xAdditionalForce = 0;
     // Force additionnelle de collision sur l'axe y
     this.yAdditionalForce = 0;
 }

 //Gestion des déplacements de la voiture selon les touches du claviers enfoncées
 function moveCar(car) {
 	// Rotation
 	car.rotation = (car.rotation + car.rotationModification * 3)%360;
    // Déplacement en x
    car.x =  car.x + (car.speed*car.speedModification) * Math.cos(Math.PI/180 * car.rotation) + car.xAdditionalForce;
    //Déplacement en y
    car.y = car.y + (car.speed*car.speedModification) * Math.sin(Math.PI/180 * car.rotation) + car.yAdditionalForce;  
 }

 //Fonction qui déplace toutes les voitures, appelée dans la boucle de jeu
 function moveCars(){
 	for (var carNumber in cars){
 		if(cars[carNumber] != undefined){
 			moveCar(cars[carNumber]);
 			//Mise à jour des forces de collision
 			updateAdditionalForces(cars[carNumber]);
 		}
 	}
 }

 	//Fonction permettant de mettre à jour les forces extérieures de collision appliquées à la voiture
	function updateAdditionalForces(car){
		var limit = 0.5;
		//On diminue constamment la force en x, et on l'annule si elle est inférieure à une limite
		if(-limit < car.xAdditionalForce && car.xAdditionalForce < limit){
			car.xAdditionalForce = 0;
		}else{
			car.xAdditionalForce = car.xAdditionalForce - car.xAdditionalForce/25;
		}

		//On diminue constamment la force en y, et on l'annule si elle est inférieure à une limite
		if(-limit < car.yAdditionalForce && car.yAdditionalForce < limit){
			car.yAdditionalForce = 0;
		}else{
			car.yAdditionalForce = car.yAdditionalForce - car.yAdditionalForce/25;
		}
	}

 //Fonction qui réinitialise la position de l'ensemble des voitures connectées
 function reinitialisation(){
    for (carNumber in cars){
    	if(cars[carNumber] != undefined){
    		cars[carNumber].speed = carSpeed;
    		cars[carNumber].isAlive = true;
    		cars[carNumber].speedModification = 1;
    		cars[carNumber].xAdditionalForce = 0;
    		cars[carNumber].yAdditionalForce  =0;
	    	//Placement des voitures selon leur ID
	    	switch(cars[carNumber].id){
		   		case 0:
		   			cars[carNumber].x = 35;
		   			cars[carNumber].y = 250;
		   			cars[carNumber].rotation = 0;
		   			break;
		   		case 1:
		   			cars[carNumber].x = 765;
		   			cars[carNumber].y = 250;
		   			cars[carNumber].rotation = 180;
		   			break;
		   		case 2:
		   			cars[carNumber].x = 390;
		   			cars[carNumber].y = 40;
		   			cars[carNumber].rotation = 90;
		   			break;
		   		case 3:
		   			cars[carNumber].x = 390;
		   			cars[carNumber].y = 460;
		   			cars[carNumber].rotation = 270;
		   			break;
		   		default:
		   			//On place la voiture au dessus ou en dessous de la première voiture afin que
	    			//les voitures n'apparaissent pas à la même hauteur les unes des autres...
		   			cars[carNumber].x = 35;
		   			cars[carNumber].rotation = 0;

	    			if (cars[carNumber].id % 2 == 0){
	    				//Si la voiture a un id pair, on la place au dessus
	    				cars[carNumber].y = 250 + (clients.length - 4) * 60; 
	    			}else{
	    				cars[carNumber].y = 250 - (clients.length - 4) * 60;
	    			}
	    			break;
	    		}
	   		}
	   	}
	// On envoie à tous les clients qu'ils doivent redessiner les voitures
   	new Message('DessinerVoitures', cars).toAll().send();
}

// Détection des collisions entre les voitures
function detectPlayersCollision(){
	var collisionDetected = false;
	for (var i=0; i<cars.length; i++){
		if(cars[i] != undefined){
			// La hitbox est un cercle
			var circle1 = {radius: 30, x: cars[i].x, y: cars[i].y};
			for(var j=i+1; j<cars.length; j++){
				if(cars[j] != undefined){
					var circle2 = {radius: 30, x: cars[j].x, y: cars[j].y};

					var dx = circle1.x - circle2.x;
					var dy = circle1.y - circle2.y;
					var distance = Math.sqrt(dx * dx + dy * dy);

					// Si une collision est detectée entre les 2 hitbox
					if (distance < circle1.radius + circle2.radius) {
					    collisionDetected = true;
						
						//On calcule les force d'opposition
						var factor = 5;
						var deltaX1 = (cars[j].x - cars[i].x)/distance * factor;
						var deltaY1 = (cars[j].y - cars[i].y)/distance * factor;

						var deltaX2 = (cars[i].x - cars[j].x)/distance * factor;
						var deltaY2 = (cars[i].y - cars[j].y)/distance * factor;

						//On ajoute ces forces, dépendantes de la vitesse des voitures en collision
						cars[i].xAdditionalForce = deltaX2*cars[j].speedModification;
						cars[i].yAdditionalForce = deltaY2*cars[j].speedModification;
						cars[j].xAdditionalForce = deltaX1*cars[i].speedModification;
						cars[j].yAdditionalForce = deltaY1*cars[i].speedModification;
					}
				}
			}
		}
	}
	// On retourne true s'il y a eu au moins une collision
	return collisionDetected;
}

// Détecte s'il y a eu collision avec le bords du canvas de jeu
function detectBordersCollision(){
		var collisionDetected = false;
		for(carNumber in cars){
			if(cars[carNumber] != undefined && cars[carNumber].isAlive == true){
				// S'il y a collision avec le bord
				if(cars[carNumber].x >= canvasWidth || cars[carNumber].x <= 0 || cars[carNumber].y >= canvasHeight || cars[carNumber].y <= 0){
		            // On envoie au client qui a fait la collision qu'il a perdu
		            new Message("Perdu", carNumber).to(clients[carNumber]).send();
		            // Sa voiture n'est plus en course
		            cars[carNumber].isAlive = false;
		        	collisionDetected = true;
		        }
		    }
		}
		// retourne s'il y a eu au moins une collision
	    return collisionDetected;
}
// La boucle de jeu s'exécute toutes les 10 millisecondes
setInterval(gameLoop, 10);


