Le code des clients est situ� dans le dosier "public" de cette archive. On y trouve donc le fichier HTML (client.html) et le script du client (client.js), ainsi que les images des voitures. De plus, on y trouve �galement la version 2.1.0 de Jquery.

Le code du serveur est situ� � la racine du dossier, dans le fichier "server.js".

Pour lancer le serveur, il faut utiliser l'interpr�teur de commande de node js, puis taper "node chemin jusque server.js". Si vous �tes situ�s dans le bon dossier, tapez tout simplement "node server.js".

Apr�s s'etre connect� en localhost ou � l'adresse IP de la machine serveur, il faut retaper l'adresse IP en question.

Enfin, un compte-rendu d�taillant le fonctionnement du projet est �galement situ� � la racine du dossier.